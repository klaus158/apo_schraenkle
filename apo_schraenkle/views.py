from django.shortcuts import render, redirect, get_object_or_404
from . import models
from bs4 import BeautifulSoup
import requests


# Create your views here.

def preisabfrage(pzn, mediliste):
    #PZN ist eine Nummer
    # Hier die Infos Scrapen...
    # https://www.aponeo.de/suche/?q=-01288865
    infos = {}
    url=f"https://www.aponeo.de/suche/?q={pzn}"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    rezeptpflichtig = False
    # Bezeichnung von Aponeo: 1. Bezeichnung Treffer, 2. Bezeichnung Privatrezept
    try:
        bezeichnung = soup.select_one("h1.apn-product-detail-title").string.strip()  # soup.h1
    except:
        try:
            bezeichnung = soup.select_one("div.apn-product-list-product-name").string.strip()
            
        except:
            bezeichnung = f"PZN: {pzn}: Nicht ermittelbar!"
    print("Bezeichnung:", bezeichnung)
    infos["bezeichnung"] = bezeichnung
    # Preis Aponeo (vor Apothekenpreis)
    try:
        preis_aponeo = soup.select_one("div.specification_price").string.replace('€', '').strip() + " €" # soup.h1
    except:
        preis_aponeo = "n/a"

    # Apothekenpreis
    try:
        preis_apotheke = soup.select_one("span.apn-product-detail-strikethrough-price-amount").string.replace('€', '').strip() + " €" # soup.h1
    except:
        try:
            preis_apotheke = soup.select_one("span.apn-product-list-price-info").string.replace('€', '').strip() + " €"
            rezeptpflichtig = True
            preis_aponeo = "-" #soup.select_one("span.apn-product-list-price-info").string.replace('€', '').strip() + " €"
        except:
            preis_apotheke = "Nicht ermittelbar!"
    print("Apotheke vor Ort:", preis_apotheke)
    
    
    print("Aponeo:", preis_aponeo)
    print("Apotheke:", preis_apotheke)

    # Shop Apotheke
    url_sa=f"https://www.shop-apotheke.com/search.htm?i=1&q={pzn}"
    response_sa = requests.get(url_sa)
    soup_sa = BeautifulSoup(response_sa.text, 'html.parser')
    try:
        preis_shopapotheke = soup_sa.select_one("span.a-Price").string.replace('€', '').strip() + " €"
        preis_shopapotheke2 = soup_sa.find_all("span",{"class":"a-Price"})[2].string.replace('€', '').strip() + " €"
        if rezeptpflichtig:
            preis_shopapotheke2 = "-"

    except:
        preis_shopapotheke = "n/a"
        preis_shopapotheke2 = "n/a"
    print("Shop-Apotheke: ", preis_shopapotheke) 
    print("Shop-Apotheke2: ", preis_shopapotheke2) 


    # eurapon
    url=f"https://www.eurapon.de/search?sSearch={pzn}"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    #context["preis_eurapon"] = soup.select_one("span.jsPrice").string.replace('€', '').strip() + " €"
    #print(context["preis_eurapon"]) 

    mediliste.append( [ bezeichnung, preis_apotheke, preis_aponeo, preis_shopapotheke2 ] )


    return infos, mediliste


def index(request):
    context = {}
    context["medikamentenliste"] = models.Medikament.objects.all()
    context["standort_zuletzt"] = "n/a"
    return render(request, "apo_schraenkle\index.html", context)

def change_mediliste(request, mid):
    medikament = get_object_or_404(models.Medikament, id=mid)
    medikament.mediliste = not medikament.mediliste
    medikament.save()
    return redirect("index")

def preisvergleich(request):
    context = {}
    try:
        preise_scrapen = models.Medikament.objects.filter(mediliste=True)
        preisliste = [["Medikament", "Preis Apotheke", "Preis Aponeo", "Preis Shop-Apotheke"] ]
        for medi in preise_scrapen:
            temp, preisliste = preisabfrage(medi.pzn, preisliste)

        context["preisliste"] = preisliste
    except:
        pass
    return render(request, "apo_schraenkle\preisvergleich.html", context)

def quick_scrape(request):
    context = {}
    pzn = "keine Eingabe!"
    infos = {}
    if request.POST:
        pzn = request.POST["pzn"]
        print("PZN übergebener Parameter: '" + pzn + "'")
        if len(pzn) > 1:
            if pzn[0] == "-":
                for i in pzn:
                    print(i, ord(i))
                pzn = pzn.replace("-", '') # das Minus abschneiden
                print("PZN Minus abgeschnitten: ", pzn)
            else:
                #pzn = "PZN ohne Minus"
                print("PZN kein Minus detected: ", pzn)
                pass
            # Prüfen, ob der PZN-String nur aus Zahlen besteht:
            mediliste = [["Medikament", "Preis Apotheke", "Preis Aponeo", "Preis Shop-Apotheke"] ]
            
            if pzn.isdecimal():
                infos, context["mediliste"] = preisabfrage(pzn, mediliste)
                print("Infos - Bezeichnung:", infos["bezeichnung"])

            else:
                #PZN ist keine Nummer
                pzn = "Gescannte PZN '" + pzn + "' ist keine Nummer!"
        else:
            pzn = "ungültig"
    print(mediliste)

    try:
        obj = models.Medikament.objects.get(pzn=pzn)
        context["db_status"] = "PZN Bereits in Datenbank"
    except Exception as e:
        context["db_status"] = f"Neu in Datenbank eingetragen! (Prüfung: {e})"
        try:
            neues_medikament = models.Medikament()
            neues_medikament.name = infos["bezeichnung"]
            neues_medikament.pzn = pzn
            neues_medikament.standort_id = 1
            neues_medikament.mediliste = True
            neues_medikament.save()
        except Exception as e:
            context["db_status"] = f"Eintragen in die Datenbank fehlgeschlagen! Fehler: {e}"
    try:
        preise_scrapen = models.Medikament.objects.filter(mediliste=True)
        preisliste = [["Medikament", "Preis Apotheke", "Preis Aponeo", "Preis Shop-Apotheke"] ]
        for medi in preise_scrapen:
            temp, preisliste = preisabfrage(medi.pzn, preisliste)

        context["preisliste"] = preisliste
    except:
        pass
    
    context["pzn"] = pzn
    context["bezeichnung"] = infos["bezeichnung"]
    return render(request, "apo_schraenkle\quick_scrape.html", context)
