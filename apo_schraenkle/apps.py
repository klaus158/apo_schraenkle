from django.apps import AppConfig


class ApoSchraenkleConfig(AppConfig):
    name = 'apo_schraenkle'
