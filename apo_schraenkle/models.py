from django.db import models
from django.db.models.fields import DateField
import datetime

# Create your models here.


class Standort(models.Model):
    #class Schublade(models.TextChoices):
    #    DARM = "Magen / Darm / Herz"
    #    AUGEN = "Augen / Vitamine"
    #    INFEKT = "Grippaler Infekt"
    #    VERBAND = "Verbandsmaterial"
    #    AMP = "Ampullen"
    #    SCHLAZI = "Schlafzimmer"
    #    BUERO = "Büro"
    #    NA = "Nicht zugewiesen"
    # id (wird von Django erstellt)
    #name = models.CharField(max_length=25, choices=Schublade.choices, default=Schublade.NA)
    name = models.CharField(max_length=100, default="Nicht zugewiesen")
    def __str__(self):
        return f"{self.name}"

class Bestand(models.Model):
    #ablaufdatum = models.DateField(default=DateField.)
    ablaufdatum = models.DateField(default=datetime.date.today)
    anzahl = models.IntegerField(default=1)
    def __str__(self):
        return f"{self.ablaufdatum}"

class Medikament(models.Model):
    # id (wird von Django erstellt)
    pzn = models.IntegerField()
    name = models.CharField(max_length=800, default='')
    notiz = models.TextField(default='')
    standort = models.ForeignKey(Standort, on_delete=models.CASCADE, null=True)
    bestand = models.ForeignKey(Bestand, on_delete=models.CASCADE, null=True)
    mediliste = models.BooleanField(default=False)
    bild = models.ImageField(null=True)
    bild_bin = models.BinaryField(null=True)
    def __str__(self):
        return f"{self.name}"
