from django.contrib import admin

# Register your models here.
from . import models

admin.site.register(models.Standort)
admin.site.register(models.Bestand)
admin.site.register(models.Medikament)